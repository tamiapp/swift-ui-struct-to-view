//
//  StructToViewApp.swift
//  StructToView
//
//  Created by ZhouJiatao on 2020/12/16.
//

import SwiftUI

@main
struct StructToViewApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

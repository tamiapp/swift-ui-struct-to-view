//
//  JSONView.swift
//  StructToView
//
//  Created by ZhouJiatao on 2021/5/16.
//

import SwiftUI

struct JSONViewCell: View {
    @Binding var json: JSON
    let KeyPath: String
    
    private let indent: CGFloat = 12.0
    
    var mType: JSONType {
        return json[keyPath: KeyPath].type
    }
    
    var depth: Int {
        return KeyPath.KpDepth()
    }
        
    var body: some View {
        makeView()
            .padding(.leading, indent * CGFloat(depth))
    }
    
    private func makeView() -> some View {
        Group {
            if mType == JSONType.number {
                SimpleIntView(title: KeyPath, value: $json[keyPath: KeyPath].intValue)
            }
            else if mType == JSONType.string {
                SimpleTextView(title: KeyPath, value: $json[keyPath: KeyPath].stringValue)
            }
            else if mType == JSONType.bool {
                SimpleBoolView(title: KeyPath, value: $json[keyPath: KeyPath].boolValue)
            }
            else if mType == JSONType.array {
                Button("添加\(KeyPath)子元素") {
                    print("添加数组\(KeyPath)的子元素")
                }
            }
            else if mType == JSONType.dictionary{
                Text(KeyPath)
            }
        }
    }
    
}

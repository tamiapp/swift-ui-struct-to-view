//
//  SampleViews.swift
//  StructToView
//
//  Created by ZhouJiatao on 2020/12/17.
//

import SwiftUI

struct SimpleTextView: View {
    var title: String
    @Binding var value: String
    /// 是否多行
    var isMultiLine = false
    var readOnly = false
    var body: some View {
        if(isMultiLine) {
            VStack {
                Text(title)
                TextEditor(text: readOnly ? .constant(value) : $value)
                    .foregroundColor(readOnly ? Color.secondary : Color.primary)
            }
        } else {
            HStack {
                Text(title)
                TextField("", text: readOnly ? .constant(value) : $value)
                    .foregroundColor(readOnly ? Color.secondary : Color.primary)
                
            }
        }
        
    }
    
}
/**
 适用于枚举类型的选择
 示例：
 enum TT:String, Identifiable,CaseIterable, CustomStringConvertible {
 case t1 = "t1"
 case t2 = "t2"
 var id: TT {
 return self
 }
 var description: String {
 self.rawValue
 }
 }
 
 struct ContentView: View {
 @State var myEnum = TT.t1
 var body: some View {
 SamplePickerView(title: "类型", value: $myEnum, allCases: TT.allCases)
 Text("选中了 \(myEnum.description)")
 }
 }
 
 */
struct SimplePickerView<SelectionValue>: View where SelectionValue : Hashable&Identifiable&CustomStringConvertible {
    var title: String
    @Binding var value: SelectionValue
    var allCases: [SelectionValue]
    
    var body: some View {
        Picker(title, selection: $value) {
            ForEach(allCases) { c in
                Text(c.description).tag(c)
            }
        }
    }
}

struct ButtonPopoverView<Content1: View, Content2: View>: View  {
    
    let buttonContent:  ()->Content1
    let popoverContent:  ()->Content2
    
//    var onPicked: (_ index: Int)->Void
//    var initIndex: Int
    
//    init(title: String, allCases: [SelectionValue], onPicked:  @escaping (SelectionValue)->Void) {
//        self.title = title
//        self.allCases = allCases
//        self.onPicked = onPicked
//        value = State(initialValue: allCases.first!)
//    }
    @State private var isPresent = false
    var body: some View {
        buttonContent()
            .onTapGesture {
                isPresent.toggle()
            }

        .popover(isPresented: $isPresent, content: {
            popoverContent()
        })

//        Picker(title, selection: $value) {
//            ForEach(allCases) { c in
//                Text(c.description).tag(c)
//            }
//        }
//        .onChange(of: value, perform: { newValue in
//            onPicked(newValue)
//        })
    }
    
    
}


struct SimpleIntView: View {
    var title: String
    @Binding var value: Int
    var range: ClosedRange<Int> = 0...1000
    var readOnly: Bool = false
    
    var formatter: NumberFormatter {
        let v = NumberFormatter()
        v.minimum = NSNumber(value: range.lowerBound)
        v.maximum = NSNumber(value: range.upperBound)
        return v
    }
    var body: some View {
        HStack {
            HStack(alignment:.bottom, spacing: 0) {
                Text("\(title)")
                Text("(\(range.lowerBound)~\(range.upperBound))")
                    .font(.footnote)
                    .foregroundColor(.secondary)
                
            }
            TextField("", value: $value, formatter: formatter)
        }
    }
}

struct SimpleBoolView: View {
    var title: String
    @Binding var value: Bool

    var body: some View {
        Toggle(title, isOn: $value)
    }
}

/*
/// 简易图片选择
struct SimpleImageChooseView: View {
    
    /// 图片名字。必须位于 ./images/目录下
    @Binding var imageName: String
    
    var body: some View {
        ZStack {
            Image(nsImage: NSImage.create(inImagesFolder: imageName))
                .resizable()
                .aspectRatio(contentMode: .fit)
                .onTapGesture {
                    JTChooseFile.CF.chooseImage { (url, respones) in
                        if let url = url {
                            imageName = url.fileName
                        }
                    }
                }
        }
    }
}
 */

struct SimpleViews_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            
            SimpleIntView(title: "年龄", value: .constant(30))
            SimpleTextView(title: "姓名", value: .constant("aa"))
            SimpleTextView(title: "姓名2", value: .constant("aa"), isMultiLine: true)
        }
    }
}

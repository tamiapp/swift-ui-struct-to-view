//
//  ContentView.swift
//  StructToView
//
//  Created by ZhouJiatao on 2020/12/16.
//

import SwiftUI

struct ContentView: View {
    @State var json: JSON = JSON.null
    @State var kps: [String] = []
    
    var body: some View {
        VStack {
            ForEach(kps,id:\.self) { kp in
                HStack {
                    JSONViewCell(json: $json, KeyPath: kp)
                }
            }
            
            Button("提交") {
                print(json)
            }
        }
        .onAppear {
            let user1 = User(id: "44-423423", name: "haha", age: 29, loc: Location(lat: 88, long: 99), arr: [3,5,7], isVIP: false)
            
            let jsonData = try? JSONEncoder().encode(user1)
            json = try! JSON(data: jsonData!)
            
            kps = json.AllKeyPath()
        }
    }
    
}

//keypath support


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//
//  JSONView.swift
//  StructToView
//
//  Created by ZhouJiatao on 2021/5/17.
//

import SwiftUI

struct JSONView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct JSONView_Previews: PreviewProvider {
    static var previews: some View {
        JSONView()
    }
}

//
//  String+JSONKeyPath.swift
//  StructToView
//
//  Created by ZhouJiatao on 2021/5/16.
//

import Foundation


public extension String {
    /// KeyPath的深度。（从0开始）
    func KpDepth() -> Int {
        return self.components(separatedBy: ".").count
    }
    
    // 获取KeyPath最内层的一个数组。
//    func KpLastArray() -> String? {
//        guard self.contains(".0") else {
//            return nil
//        }
//        
//        //移除components的最后一个 "0"
//        let components = self.components(separatedBy: ".")
//        let index = components.lastIndex(of: "0")!
//        
//        let result = components[0..<index].joined(separator: ".")
//        
//        return result
//    }
}


//
//  SwiftyJSON+KeyPath.swift
//  StructToView
//
//  Created by ZhouJiatao on 2021/5/14.
//

import Foundation

extension JSON {

/// 自动转换成数组索引和key
    /// 如 json["0"] 实际执行 json[0]
    /// json["hello"] 实际执行 json["hello"]
    subscript(keyOrIndex keyOrIndex: String) -> JSON {
        get {
            if let num = Int(keyOrIndex) {
                return self[num]
            } else {
                return self[keyOrIndex]
            }
        }
        set {
            if let num = Int(keyOrIndex) {
                return self[num] = newValue
            } else {
                return self[keyOrIndex] = newValue
            }
        }
    }
    
    // todo 适配更深的keypath。
    // todo keyOrIndex简化一下名称 private
    subscript(keyPath keyPath: String) -> JSON {
        get {
            var r = JSON.null
            let keys: [String] = keyPath.components(separatedBy: ".")
            switch keys.count {
            case 1:
                r = self[keyOrIndex:keys[0]]
            case 2:
                r = self[keyOrIndex:keys[0]][keyOrIndex:keys[1]]
            case 3:
                r = self[keyOrIndex:keys[0]][keyOrIndex:keys[1]][keyOrIndex:keys[2]]
            case 4:
                r = self[keyOrIndex:keys[0]][keyOrIndex:keys[1]][keyOrIndex:keys[2]][keyOrIndex:keys[3]]
            case 5:
                r = self[keyOrIndex:keys[0]][keyOrIndex:keys[1]][keyOrIndex:keys[2]][keyOrIndex:keys[3]][keyOrIndex:keys[4]]
            case 6:
                r = self[keyOrIndex:keys[0]][keyOrIndex:keys[1]][keyOrIndex:keys[2]][keyOrIndex:keys[3]][keyOrIndex:keys[4]][keyOrIndex:keys[5]]
            case 7:
                r = self[keyOrIndex:keys[0]][keyOrIndex:keys[1]][keyOrIndex:keys[2]][keyOrIndex:keys[3]][keyOrIndex:keys[4]][keyOrIndex:keys[5]][keyOrIndex:keys[6]]
            case 8:
                r = self[keyOrIndex:keys[0]][keyOrIndex:keys[1]][keyOrIndex:keys[2]][keyOrIndex:keys[3]][keyOrIndex:keys[4]][keyOrIndex:keys[5]][keyOrIndex:keys[6]][keyOrIndex:keys[7]]
            case 9:
                r = self[keyOrIndex:keys[0]][keyOrIndex:keys[1]][keyOrIndex:keys[2]][keyOrIndex:keys[3]][keyOrIndex:keys[4]][keyOrIndex:keys[5]][keyOrIndex:keys[6]][keyOrIndex:keys[7]][keyOrIndex:keys[8]]
            case 10:
                r = self[keyOrIndex:keys[0]][keyOrIndex:keys[1]][keyOrIndex:keys[2]][keyOrIndex:keys[3]][keyOrIndex:keys[4]][keyOrIndex:keys[5]][keyOrIndex:keys[6]][keyOrIndex:keys[7]][keyOrIndex:keys[8]][keyOrIndex:keys[9]]
                
            default:
                assertionFailure("JSON的keypath层次太深了")
                break
            }
            
            return r
        }
        
        set {
            
            let keys: [String] = keyPath.components(separatedBy: ".")
            switch keys.count {
            case 1:
                self[keyOrIndex:keys[0]] = newValue
            case 2:
                self[keyOrIndex:keys[0]][keyOrIndex:keys[1]] = newValue
            case 3:
                self[keyOrIndex:keys[0]][keyOrIndex:keys[1]][keyOrIndex:keys[2]] = newValue
            case 4:
                self[keyOrIndex:keys[0]][keyOrIndex:keys[1]][keyOrIndex:keys[2]][keyOrIndex:keys[3]] = newValue
            case 5:
                self[keyOrIndex:keys[0]][keyOrIndex:keys[1]][keyOrIndex:keys[2]][keyOrIndex:keys[3]][keyOrIndex:keys[4]] = newValue
            case 6:
                self[keyOrIndex:keys[0]][keyOrIndex:keys[1]][keyOrIndex:keys[2]][keyOrIndex:keys[3]][keyOrIndex:keys[4]][keyOrIndex:keys[5]] = newValue
            case 7:
                self[keyOrIndex:keys[0]][keyOrIndex:keys[1]][keyOrIndex:keys[2]][keyOrIndex:keys[3]][keyOrIndex:keys[4]][keyOrIndex:keys[5]][keyOrIndex:keys[6]] = newValue
            case 8:
                self[keyOrIndex:keys[0]][keyOrIndex:keys[1]][keyOrIndex:keys[2]][keyOrIndex:keys[3]][keyOrIndex:keys[4]][keyOrIndex:keys[5]][keyOrIndex:keys[6]][keyOrIndex:keys[7]] = newValue
            case 9:
                self[keyOrIndex:keys[0]][keyOrIndex:keys[1]][keyOrIndex:keys[2]][keyOrIndex:keys[3]][keyOrIndex:keys[4]][keyOrIndex:keys[5]][keyOrIndex:keys[6]][keyOrIndex:keys[7]][keyOrIndex:keys[8]] = newValue
            case 10:
                self[keyOrIndex:keys[0]][keyOrIndex:keys[1]][keyOrIndex:keys[2]][keyOrIndex:keys[3]][keyOrIndex:keys[4]][keyOrIndex:keys[5]][keyOrIndex:keys[6]][keyOrIndex:keys[7]][keyOrIndex:keys[8]][keyOrIndex:keys[9]] = newValue
                
            default:
                assertionFailure("JSON的keypath层次太深了")
                break
            }
            
        }
    }
    
    func AllKeyPath() -> [String] {
            
        var kps: [String] = []
        var arrOrDics: [(context: String,value: JSON)] = [("",self)]
            
        while let (context,arrOrDic) = arrOrDics.popLast() {
                for (key, element) in arrOrDic {
                    if element.type == .null || element.type == .unknown {
                        continue
                    }
                    
                    let currentKp = context.isEmpty ? key : "\(context).\(key)"
                    
                    if element.type == .array || element.type == .dictionary {
                        kps.append(currentKp)
                        arrOrDics.append((currentKp, element))
                    } else {
                        kps.append(currentKp)
                    }
                }
                
            }
            
        return kps.sorted()
    }
    
    // 获取所有数组KeyPath
//    func AllArrayKeyPath() -> [String] {
//
//        let all = AllKeyPath()
//        var result: [String] = []
//        for kp in all {
//
//            if let lastArray = kp.KpLastArray() {
//                result.append(lastArray)
//            }
//
//        }
//        return result
//    }
    

    
}
